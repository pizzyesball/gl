const editors = require('../node_modules/tzar-spreadsheet-to-config/src/editors/editors');
const app = require('../node_modules/tzar-spreadsheet-to-config/src/app.js');
const auth = require('../sheets.googleapis.com-tzar.json');

// Ссылка на ГуглТаблицу
const spreadSheetUrl = 'https://docs.google.com/spreadsheets/d/1SH1vyUwTelQxMglZCGA67taVqoBPym-ZOvWb-oo90cA/edit#gid=0';

// access_token из файла sheets.googleapis.com-tzar.json после запуска команды yarn google-auth
const apiKey = auth.access_token;

const configFolder = `${__dirname}`; // Путь до папки с конфигами
const projectRoot = `${__dirname}/..`; // Путь до корневого католого проекта
const tzarComponents = `${__dirname}/../node_modules/tzar-components/src`; // Путь до папки с компонентами tzar-components

/**
 * Здесь нужно перечислить конфиги, которые нужно сгенерировать.
 * Каждый массив состоит из трех аргументов
 * 1. Полный путь, откуда брать оригинальный конфиг
 * 2. Функция, которая будет редактировать конфиг
 * 3. Полный путь, куда сохранять отредактированный конфиг.
 * 	  Путь может быть строкой, а может быть массивом строк.
 *    Если это массив, то отредактрованный конфиг можно сохранить в несколько папок.
 */
app.run(spreadSheetUrl, apiKey, 0, ['Конфигурация', 'TRUE', 'Вопрос', 'Ответ'], [
	// [`${configFolder}/sites.yaml`, editors.sitesYaml, [`${configFolder}/sites.yaml`]],
	// [`${configFolder}/zgettext.yaml`, editors.zgetText],
	// [`${configFolder}/styles.yaml`, editors.stylesYaml],
	// [`${configFolder}/search.yaml`, editors.searchYaml],
	// [`${configFolder}/sites.yaml`, editors.sitesYaml, [`${configFolder}/sites.yaml`, `${projectRoot}/example/config/sites.yaml`]],
	// [`${configFolder}/structure.yaml`, editors.structureYaml],
	// [`${configFolder}/email.yaml`, editors.emailYaml],
	// [`${tzarComponents}/title/.variant/base/text.yaml`, editors.titleText, `${projectRoot}/src/components/title/.variant/base/text.yaml`],
	// [`${tzarComponents}/buttons-panel/.variant/in-column/config.yaml`, editors.buttonsPanelColumnConfig, `${projectRoot}/src/components/buttons-panel/.variant/in-column/config.yaml`],
	// [`${tzarComponents}/copyright/.variant/base/config.yaml`, editors.copyrightConfig, `${projectRoot}/src/components/copyright/.variant/base/config.yaml`],
	// [`${tzarComponents}/copyright/.variant/base/text.yaml`, editors.copyrightText, `${projectRoot}/src/components/copyright/.variant/base/text.yaml`],
	// [`${tzarComponents}/report-selector/.variant/drop-down/text.yaml`, editors.reportSelectorDropDownText, `${projectRoot}/src/components/report-selector/.variant/drop-down/text.yaml`],
	// [`${tzarComponents}/report-selector/.variant/multi-button/text.yaml`, editors.reportSelectorMultiButtonText, `${projectRoot}/src/components/report-selector/.variant/multi-button/text.yaml`],
	// [`${tzarComponents}/cookies-policy/.variant/base/text.yaml`, editors.cookiesPolicyText, `${projectRoot}/src/components/cookies-policy/.variant/base/text.yaml`],
	// [`${tzarComponents}/social-services/.variant/compact/config.yaml`, editors.socialServiceCompactConfig, `${projectRoot}/src/components/social-services/.variant/compact/config.yaml`],
	// [`${tzarComponents}/social-services/.variant/static-in-line/config.yaml`, editors.socialServiceInlineConfig, `${projectRoot}/src/components/social-services/.variant/static-in-line/config.yaml`],
	// [`${__dirname}/../package.json`, editors.packageJson],
]);

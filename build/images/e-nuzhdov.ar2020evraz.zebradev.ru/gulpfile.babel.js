const gulp = require('gulp');
const chalk = require('chalk');
const path = require('path');
const argv = require('yargs').argv;
const fileExists = require('file-exists').sync;
const del = require('del');

if (!argv.dirty) {
	// Очистка temp
	del.sync('temp/**/*');
}

let componentsPath = null;

if (argv.dev) {
	// Смотрим, есть ли проект tzar-components рядом с текущим (и игнорируем его, если мы в GitLab CI)
	const tzarComponentsProjectExists = fileExists('../tzar_modules/tzar-components/package.json') && !process.env.CI;
	// Если проект есть, используем пути к компонентам относительно него
	if (tzarComponentsProjectExists) {
		componentsPath = path.resolve('../tzar_modules/tzar-components/src');
	}
}

//подключение ядра
const tzarCore = require('tzar-core');

//если на уровне с шаблоном есть components, то подключаем его, иначе подключаем из node_modules
const componentsPackageJson = componentsPath ? require(path.resolve(componentsPath, '../package.json')) : require('tzar-components/package.json');
console.log(chalk.blue(`Версия ${componentsPackageJson.name}: `) + chalk.bold(componentsPackageJson.version));

const config = tzarCore.config;
if (!config.production) {
	// config.load('paths', path.resolve('.') + '/config/paths.*');
	// Если запуск не на проде, и в папке example есть конфиги, загружаем сперва их
	config.loadFolder(path.resolve('.') + '/example/config');
}
config.loadFolder(path.resolve('.') + '/config');

tzarCore.initTasks(gulp, config, componentsPath);

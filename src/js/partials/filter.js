if ($('.js-filter').length) {

    $(document).on('click', '.js-filter', function () {
        var $scrollBar = $(this).parent().find('.js-scroll-block');

        $(this).addClass('is-expand').next().addClass('is-expand');

        $('body').addClass('no-scroll');
        // if (windowsOS) {
        //     $body.addClass('windows');
        // }

        $scrollBar.overlayScrollbars({
            overflowBehavior: {
                x: 'hidden',
                y: 'scroll'
            }
        });
    });

    $(document).on('click', '.js-filter-close', function () {
        $('.js-filter').removeClass('is-expand').next().removeClass('is-expand');

        $('body').removeClass('no-scroll windows');
    });

    $(document).on('click', '.js-filter-link', function () {

        var $checkbox = $(this).children('input');

        if ($checkbox.is(':checked')) {
            $checkbox.attr('checked', 'checked');
        } else {
            $checkbox.removeAttr('checked');
        }

        $(this).toggleClass('is-select');

        if ($('.is-select').length > 0) {
        	$('.js-filter-clear').removeClass('disabled');
        } else {
        	$('.js-filter-clear').addClass('disabled');
        }
    });

    $(document).on('click', '.js-filter-clear', function () {
        $('.js-filter-link').removeClass('is-select');
        $('.js-filter-link').children('input').removeAttr('checked');
        $(this).addClass('disabled');
    });

    $(document).on('click', '.js-filter-apply', function () {
        $('.js-filter').removeClass('is-expand').next().removeClass('is-expand');
        $('body').removeClass('no-scroll windows');
    });

    $(document).on('click', '.js-filter-nav', function () {
        $(this).addClass('active').children('input').attr('checked', 'checked');
    });

}

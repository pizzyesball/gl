if ($('.js-directions-slider').length) {

    $('.js-directions-slider').each(function () {

        var $slider = $(this),
            $dots = $slider.siblings('.js-directions-nav'),
            $slideDots = $dots.find('.slick-dots'),
            center = false;

        if ($slider.parent().parent().hasClass('b-directions--about')) {
            center = true;
        }

        $slider.slick({
            centerMode: center,
            centerPadding: 0,
            variableWidth: true,
            rows: 0,
            slidesToScroll: 1,
            nextArrow: $slider.siblings().find('.js-next'),
            prevArrow: $slider.siblings().find('.js-prev'),
            infinite: false,
            dots: true,
            appendDots: $dots,
            customPaging: function (slider, i) {
                return '<button class="dot">' + $(slider.$slides[i]).attr('title') + '</button>';
            }
        });

        $slider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
            var $elem;
            var left = 0;
            var lastLeft = 0;
            var right = 0;

            $slideDots = $dots.find('.slick-dots');
            $elem = $slideDots.children().filter(function (index) {
                return index < nextSlide;
            });

            if (nextSlide === $slideDots.children().length - 1 && $elem.length && window.innerWidth < 767) {
                lastLeft = $dots.scrollLeft();
                $elem.each(function () {
                    left += $(this).outerWidth(true);
                });

                $dots.scrollLeft(left);
                right = left - $dots.scrollLeft();
                $dots.scrollLeft(lastLeft);

                $dots.stop().animate({
                    scrollLeft: left,
                    right: right
                }, 250);
            } else if ($elem.length) {
                $elem.each(function () {
                    left += $(this).outerWidth(true);
                });

                $dots.stop().animate({
                    scrollLeft: left,
                    right: 0
                }, 250);
            } else {
                $dots.stop().animate({
                    scrollLeft: 0,
                    right: 0
                }, 250);
            }
        });
    });
}



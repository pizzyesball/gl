var $header = $('header'),
    $menu = $('.js-menu'),
    $menuLink = $('.js-menu-link'),
    $menuItem = $menuLink.parent(),
    $menuWrap = $menu.children('.b-menu__wrap-bg'),
    $menuLine = $('.js-menu-line'),
    $menuActiveLink,
    menuLineWidth,
    menuActivePos;

function menuRefresh() {
    $menuActiveLink = $menu.find('.is-expand').children();
    menuLineWidth = $menuActiveLink.width();
    menuActivePos = $menuActiveLink.position().left;

    $menuLine.css({
        left: menuActivePos - 5,
        width: menuLineWidth + 10
    });
}

$menuWrap.css({
    height: $header.outerHeight(),
    opacity: 0
});

$(window).scroll(function () {
    var height = $(window).scrollTop();
    var heightToggle = $header.outerHeight() / 2;

    if (height > heightToggle) {
        $header.addClass('header-color');
    } else {
        $header.removeClass('header-color');
    }
});

// перекрашивание меню
$menu.hover(function () {

    if ($header.hasClass('hover-menu')) {
        $header.removeClass('hover-menu');
        $menuItem.removeClass('is-expand');
        $('.b-menu__wrap').css('height', '');
        $menuWrap.css({
            height: $header.outerHeight(),
            opacity: 0
        });
        $menuLine.css({
            width: 0
        });
    } else {
        $header.addClass('hover-menu');
    }
});

$menuItem.hover(function () {
    var $menuWrapContent = $(this).children('.b-menu__wrap'),
        $menuContent = $menuWrapContent.children('.b-menu__wrap-inner'),
        menuContentHeight = $menuContent.outerHeight();

    if ($(this).hasClass('is-expand')) {

    } else {

        // if ($menuWrapContent.length) {
        $(this).addClass('is-expand').siblings().removeClass('is-expand').find('.b-menu__wrap').css('height', '');
        menuRefresh();
        if ($menuWrapContent.length) {
            $menuWrap.css({
                height: menuContentHeight + $header.outerHeight(),
                opacity: 1
            });

            $menuWrapContent.css('height', menuContentHeight);
        } else {
            $menuWrap.css({
                height: $header.outerHeight(),
                opacity: 1
            });

            $menuWrapContent.css('height', '');
        }
        // } else {
        //     $menuItem.removeClass('is-expand');
        //     $('.b-menu__wrap').css('height', '');
        //     menuRefresh();
        //     $menuWrap.css({
        //         height: $header.outerHeight(),
        //         opacity: 1
        //     });
        // }
    }
});

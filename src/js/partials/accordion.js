if ($('.js-acc').length) {

    $('.js-acc-header').on('click', function () {

        var $this = $(this);

        $this.toggleClass('is-expand').next().slideToggle(500, 'linear');
        $this.hasClass('is-expand') && $('html').animate({
            scrollTop: $this.offset().top - $('.b-header')[0].offsetHeight
        }, 500,'linear');
    });
}

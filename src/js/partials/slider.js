function setHeiHeight() {
    $('.b-head-block-slider, .js-full').css({
        height: $(window).height() + 'px'
    });
}

setHeiHeight();

$(window).resize(setHeiHeight);

$('.js-head-slider').each(function () {

    var $slider = $(this);

    $slider.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        fade: true,
        rows: 0,
        infinite: true,
        pauseOnHover: false,
        autoplay: true,
        autoplaySpeed: 3000,
        nextArrow: $slider.siblings().find('.js-next'),
        prevArrow: $slider.siblings().find('.js-prev')
    });

    $slider.on('afterChange', function (event, slick, currentSlide) {
        $slider.siblings().find('.js-number-active-slide').text('0' + (currentSlide + 1));
    });
});

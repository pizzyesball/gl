if ($('.js-more-slider').length) {

    $('.js-more-slider').each(function () {

        var $slider = $(this);

        $slider.slick({
            slidesToShow: 1,
            rows: 0,
            slidesToScroll: 1,
            dots: false,
            infinite: false,
            autoplay: false,
            mobileFirst: true,
            nextArrow: $slider.siblings().find('.js-next'),
            prevArrow: $slider.siblings().find('.js-prev'),
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2
                    }
                },{
                    breakpoint: 1023,
                    settings: {
                        slidesToShow: 3
                    }
                },{
                    breakpoint: 1439,
                    settings: {
                        slidesToShow: 4
                    }
                }
            ]
        });
    });
}

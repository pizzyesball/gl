var $nav = $('.js-nav'),
    $navLine = $('.js-nav-line'),
    $navActiveLi,
    navLineHeight,
    navLiPos;

function navRefresh() {
    $navActiveLi = $nav.find('.js-nav-link.active');
    navLineHeight = $navActiveLi.height();
    navLiPos = $navActiveLi.position().top;

    $navLine.css({
        top: navLiPos,
        height: navLineHeight
    });
}

function windowScroll() {
    $(window).scroll(function () {

	    var $sections = $('section');

        if ($(window).scrollTop() < $('.b-subscribe').offset().top) {

            $sections.each(function (i,el) {

    	        var sectionTop = $(el).offset().top - 50;
    	        var sectionBottom = sectionTop + $(el).height();
    	        var scroll = $(window).scrollTop();
    	        var id = $(el).attr('id');

    	    	if (scroll > sectionTop && scroll < sectionBottom) {
    	            $('.js-nav-link.active').removeClass('active');
                    $('.js-nav-link[href="#'+id+'"]').addClass('active');
                    navRefresh();
    	        }
    	    });
        } else {
            $('.js-nav-link.active').removeClass('active');
            $navLine.css({
                height: 0
            });
        }
    });
}

function navScroll() {
    $('.js-nav-link').on('click', function () {
	    event.preventDefault();
	    var id = $(this).attr('href'),
	        top = $(id).offset().top;
	    $('body,html').animate({scrollTop: top}, 700);
    });
}

if ($nav.length) {
    navScroll();
    windowScroll();

    navRefresh();
}


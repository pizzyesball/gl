$('.js-prod-slider').slick({
    slidesToShow: 3,
    rows: 0,
    slidesToScroll: 1,
    dots: false,
    arrows: false,
    infinite: true,
    autoplay: true
});

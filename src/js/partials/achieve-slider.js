$('.js-achieve-slider').each(function () {

    var $slider = $(this);

    $slider.slick({
        variableWidth: true,
        slidesToScroll: 1,
        dots: false,
        fade: false,
        rows: 0,
        infinite: true,
        nextArrow: $slider.siblings().find('.js-next'),
        prevArrow: $slider.siblings().find('.js-prev'),
        centerMode: true,
        centerPadding: '0px'
    });

    $slider.on('afterChange', function (event, slick, currentSlide) {
        $slider.siblings().find('.js-number-active-slide').text('0' + (currentSlide + 1));
    });
});

$('.js-image-slider').each(function () {

    var $slider = $(this);

    $slider.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        rows: 0,
        infinite: false,
        focusOnSelect: true,
        fade: true,
        cssEase: 'linear'
    }).on('afterChange', function (event, slick, currentSlide, nextSlide) {
        $('.js-image-dot').eq(currentSlide).addClass('on').siblings().removeClass('on');
    });

    $('.js-image-dot:first-child').addClass('on');

    $('.js-image-dot').on('click', function (e) {

        var slideIndex = $(this).data('item');
        $(this).addClass('on').siblings().removeClass('on');
        $slider.slick('slickGoTo', parseInt(slideIndex) - 1);
        e.preventDefault();

    });
});

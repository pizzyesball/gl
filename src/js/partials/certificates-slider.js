$('.js-certificates-slider').each(function () {

    var $slider = $(this);

    $slider.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        infinite: false,
        mobileFirst: true,
        rows: 0,
        nextArrow: $slider.siblings().find('.js-next'),
        prevArrow: $slider.siblings().find('.js-prev'),
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    variableWidth: true,
                    slidesToShow: 2
                }
            }
        ]
    });
});

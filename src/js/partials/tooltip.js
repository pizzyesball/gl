$('.js-tooltip').tooltipster({
    arrow: false,
    distance: 0,
    side: 'top',
    delay: 0,
    minWidth: 288,
    trigger: ('ontouchstart' in window) ? 'click' : 'hover'
});

$('.js-tooltip-geography').tooltipster({
    theme: 'tooltipster-geography',
    arrow: false,
    distance: -15,
    delay: 0,
    side: 'top',
    trigger: ('ontouchstart' in window) ? 'click' : 'hover'
});

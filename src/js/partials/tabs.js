var $tabs = $('.js-tab'),
    $tabLine = $('.js-tab-line'),
    $tabActiveLi,
    tabLineWidth,
    tabNavPos,
    tabLiPos;

function tabRefresh() {
    $tabActiveLi = $tabs.find('.js-tab-trigger.active');
    tabLineWidth = $tabActiveLi.outerWidth();
    tabLiPos = $tabActiveLi.position().left;
    tabNavPos = $tabs.position().left;
}

function tabLineSet() {
    $tabLine.animate({
        left: tabLiPos - tabNavPos,
        width: tabLineWidth
    }, 300);
}

if ($tabs.length) {

    tabRefresh();

    tabLineSet();

    $tabs.find('.js-tab-trigger').on('click', function () {
        $tabActiveLi.removeClass('active');
        $(this).addClass('active');
        tabRefresh();
        tabLineSet();
    });

    $('.js-tab-trigger').click(function () {
        var id = $(this).attr('data-tab'),
            content = $('.js-tab-content[data-tab="'+ id +'"]');

        $('.js-tab-trigger.active').removeClass('active');
        $(this).addClass('active');

        $('.js-tab-content.active').removeClass('active');
        content.addClass('active');
    });
}

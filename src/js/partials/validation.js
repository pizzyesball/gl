$(document).ready(function () {
    var SETTINGS = {
        ignore: '.ignore, [type="hidden"]',

        // Установка класс "error"
        highlight: function (element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
        },

        // Удаление класс "error"
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass(errorClass).addClass(validClass);
            $(element).attr('placeholder', '');
        },
        errorPlacement: function (error, element) {
            $(element).attr('placeholder', $(error).html());
        }
    };

    $.validator.setDefaults({debug: true});
    $.extend($.validator.messages, {
        required: 'Это поле необходимо заполнить.',
        name: 'Укажите имя',
        tel: 'Укажите телефон',
        remote: 'Пожалуйста, введите правильное значение.',
        email: 'Укажите электронный адрес',
        url: 'Пожалуйста, введите корректный URL.',
        date: 'Пожалуйста, введите корректную дату.',
        dateISO: 'Пожалуйста, введите корректную дату в формате ISO.',
        number: 'Пожалуйста, введите число.',
        digits: 'Пожалуйста, вводите только цифры.',
        creditcard: 'Пожалуйста, введите правильный номер кредитной карты.',
        equalTo: 'Пожалуйста, введите такое же значение ещё раз.',
        extension: 'Пожалуйста, выберите файл с правильным расширением.',
        maxlength: $.validator.format('Пожалуйста, введите не больше {0} символов.'),
        minlength: $.validator.format('Пожалуйста, введите не меньше {0} символов.'),
        rangelength: $.validator.format('Пожалуйста, введите значение длиной от {0} до {1} символов.'),
        range: $.validator.format('Пожалуйста, введите число от {0} до {1}.'),
        max: $.validator.format('Пожалуйста, введите число, меньшее или равное {0}.'),
        min: $.validator.format('Пожалуйста, введите число, большее или равное {0}.')
    });

    $.validator.addMethod('email', function (value, element) {
        // var regEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        // if ($(element).hasClass('js-email')) {
        //     if ($(element).prop('required') || value.replace(/\s/g, '').length) {
        //         return regEmail.test(value);
        //     } else {
        //         return true;
        //     }
        // } else {
        //     return true;
        // }
        var regEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regEmail.test(value);
    }, $.validator.messages.email);

    // Телефон
    $.validator.addMethod('tel', function (value, element) {
        // var regPhone = /^((\+7|7|8)+([0-9]){10})$/;
        var regPhone = /^(\+7|7|8)?[\s\-]?\(?[0-9][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/;
        return regPhone.test(value);
    }, $.validator.messages.tel);

    // function serializeFormData ($form) {
    //     var formData = new FormData($form);

    //     return formData;
    // }

    $('body').on('initValidation', function () {
        $('.js-form-validate').each(function () {
            var $this = $(this);
            var localSettings = {};
            var settings = $.extend(localSettings, SETTINGS);
            var validator = $this.validate(settings);

            // if ($this.attr('name')=='contacts'){
            //     localSettings.errorPlacement = function(error, element) {
            //         element.attr('placeholder', error.text());
            //     }
            // }

            // $this.on('reset', function (event) {
            //     validator.resetForm();
            // });

            $this.on('submit', function (e) {
                var $fileInput = $this.find('input[type="file"]');
                var $popupButton = $this.find('[data-popup]');
                var event = $this.attr('data-event');
                if (validator.numberOfInvalids() > 0) {
                    e.stopImmediatePropagation();

                } else {
                    var $form = $this;
                    var ajaxSettings = {
                        url: $form.attr('action') || location.href,
                        type: $form.attr('method') || 'get',
                        success: function (data) {
                            if (event && data) {
                                $('body').trigger(event, data);
                                $form.get(0).reset();
                                validator.resetForm();
                                $form.find('label[for="file"]').text('+ ПРИКРЕПИТЬ ФАЙЛ');

                                if ($popupButton.length) {
                                    $('body').trigger('popup-trigger.open', [$popupButton.get(0)]);
                                }
                            }
                        },
                        error: function () {
                            console.error('error');
                        }
                    };

                    if ($fileInput.length) {
                        ajaxSettings = $.extend(ajaxSettings, {
                            data: new FormData(this),
                            cache: false,
                            contentType: false,
                            processData: false
                        });

                    } else {
                        ajaxSettings = $.extend(ajaxSettings, {
                            data: $form.serializeArray(),
                            dataType: 'json'
                        });
                    }

                    $.ajax(ajaxSettings);
                }
            });

            $this.find('.js-check-form').on('click', function (event) {
                var _this = this;
                var $form = $(_this).parents('form');
                if (validator.numberOfInvalids()) {
                    return;
                }
                validator.form();
                $form.submit();
                setTimeout(function () {
                    if (validator.numberOfInvalids() == 0) {
                        $('body').trigger('popup-trigger.open', [_this]);
                    }
                }, 1000);
            });
        });
    });

    $('body').trigger('initValidation');
});

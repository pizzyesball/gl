$('.js-wwu-slider').each(function () {

    var $slider = $(this);

    $slider.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        infinite: false,
        rows: 4,
        mobileFirst: true,
        nextArrow: $slider.siblings().find('.js-next'),
        prevArrow: $slider.siblings().find('.js-prev'),
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    rows: 2
                }
            },{
                breakpoint: 1023,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    rows: 1
                }
            }
        ]
    });
});

$(function () {
    $('.js-before-after').twentytwenty({
        default_offset_pct: 0.3,
        orientation: 'horizontal',
        no_overlay: true
    });
});

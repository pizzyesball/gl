// =============
// Pages used: contacts.html, partner-map.html, company.html
// =============
$(function () {
    var url = $('#map').length && $('#map').attr('data-url');
    var placemarksCollection;
    var myMap;
    var items;
    var mapScript = $('script[data-map-src]')[0];
    var $curtain = $('.js-page-change');

    var startedFlag = false;


    // отложенная после анимаций загрузка скриптов
    if (mapScript && Boolean(url)) {
        setTimeout(startLoad, 4000); // загрузка все равно начнется максимум после 4 секунд
        if ($curtain.length && $curtain.css('display') !== 'none') {
            $curtain.on('animationend', startLoad);
        } else {
            startLoad();
        }
        function startLoad() {
            mapScript.onload = () => ymaps.ready(init);
            mapScript.src = mapScript.dataset.mapSrc;
        }
    }

    function init() {
        if (startedFlag) {
            return;
        }
        startedFlag = true;

        var placemark;
        var zoomControl = new ymaps.control.ZoomControl({
            options: {
                suppressMapOpenBlock: true,
                size: 'small',
                position: {
                    bottom: 15,
                    right: 15
                }
            }
        });

        $.get(url, function (json) {
            var points = json.data;
            var firstItem = points[Object.keys(points)[0]];
            var placemarks = {};
            items = json.data.items;
            myMap = new ymaps.Map('map', {
                center: firstItem.coordinate,
                zoom: 7,
                controls: [zoomControl],
            }, {
                suppressMapOpenBlock: true,
                yandexMapDisablePoiInteractivity: true
            });
            myMap.behaviors.disable(['scrollZoom']);
            
            myMap.controls.add('routePanelControl');

            var routePanel = myMap.controls.get('routePanelControl');
            routePanel.options.set({
                visible: false,
                allowSwitch: false
            });

            routePanel.routePanel.state.set({
                fromEnabled: true,
                to: "Московская область, городской округ Мытищи, деревня Ерёмино, Дмитровское шоссе, 100Ж",
                toEnabled: false,
                type: "auto"
            });

            $('.js-route').on('click', function () {
                routePanel.options.set({
                    visible: true,
                    allowSwitch: false
                });
                routePanel.routePanel.geolocate('from');
            });

            $('#map').on('click', '.js-route', function () {
                routePanel.options.set({
                    visible: true,
                    allowSwitch: false
                });
                routePanel.routePanel.geolocate('from');
            });

            Object.keys(points).forEach(name => {
                var point = points[name];
                placemarks[name] = new ymaps.Placemark(point.coordinate, {
                    balloonContentHeader: point.header,
                    balloonContentBody: point.body,
                    balloonContentFooter: point.footer
                }, getIcon(point));
                placemarks[name].events.add('balloonopen', function (e) {
                    var $root = $('.ymaps-2-1-78-balloon-panel');
                    var $baloon = $('.ymaps-2-1-78-balloon');
                    var diff = $('#map').offset().top - $baloon.offset().top;

                    if (diff > 0){
                        $root.css({top:diff});
                    }
                })
                myMap.geoObjects.add(placemarks[name]);
            });

            myMap.setBounds(myMap.geoObjects.getBounds(), {checkZoomRange: true, zoomMargin: 10});

            myMap.events.add('click', e => e.get('target').balloon.close());

            var openPlaceMark = function (pm) {
                $('html, body').animate({
                    scrollTop: $('#map').offset().top - $('.b-header')[0].offsetHeight
                },{
                    duration: 500
                });
                if (!pm && !placemarks[pm]) {
                    return;
                }
                placemarks[pm].balloon.open();
                window.innerWidth > 768 && myMap.setCenter(points[pm].coordinate);
            }
            $(document).on('click', 'a[href*="#map"]', function (e) {
                e.preventDefault();
                let pm = this.dataset.placemark;
                openPlaceMark(pm);
            });

            $(document).ready(function () {
                if (location.hash.indexOf('::')){
                    let hash = data = location.hash.split('::');
                    let data = hash[1];
                    location.hash = hash[0];
                    data && setTimeout(function () {
                        openPlaceMark(data)
                    }, 400);

                }
            });
        });
    }

    function getIcon(point) {
        return $.extend(point.img ? {
            iconLayout: 'default#image',
            iconImageHref: point.img,
            iconImageSize: [48, 48]
        } : {
            preset: 'islands#icon',
            iconColor: '#F33939'
        }, {
            hideIconOnBalloonOpen: false,
            balloonOffset: [-100, 10]
        });
    }

});

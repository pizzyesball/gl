if ($('.js-val-link').length) {

    $('.js-val-link').on('click', function () {

        var $this = $(this),
        	id = $this.attr('data-id');

        if ($this.hasClass('active')) {

        } else {
            $this.addClass('active').parent().siblings().children().removeClass('active');
            $('.js-val-item[data-content=' + id + ']').addClass('active').siblings().removeClass('active');
        }
    });

    $('.js-val-link').hover(function () {
    	$(this).trigger('click');
    });
}

$('.js-burger').on('click', function () {

    var $burgerButton = $(this),
        $burgerMenu = $burgerButton.next();

    if ($burgerButton.hasClass('is-expand')) {

        $('body').removeClass('no-scroll');
        $('.b-header .b-logo').removeClass('color');
        $burgerButton.removeClass('is-expand');
        $burgerMenu.removeClass('is-expand');
        $burgerMenu.delay(500).queue(function (next) {
            $burgerMenu.find('.is-expand').removeClass('is-expand');
            $burgerMenu.find('.no-show').removeClass('no-show');
            $burgerMenu.children().removeClass('open');
            $burgerMenu.hide();
            next();
        });
    } else {

        $('body').toggleClass('no-scroll');
        $('.b-header .b-logo').toggleClass('color');
        $burgerButton.toggleClass('is-expand');
        $burgerMenu.show();
        $burgerMenu.delay(50).queue(function (next) {
            $burgerMenu.addClass('is-expand');
            next();
        });
    }
});

$('.js-burger-link').on('click', function () {

    $('.b-burger__list').addClass('open');

    if ($(this).hasClass('is-expand')) {
    	$('.js-burger-link').removeClass('is-expand').next().removeClass('is-expand');
    	$('.js-burger-link').removeClass('no-show');
    	$('.b-burger__list').removeClass('open');
    } else {
        $(this).addClass('is-expand').removeClass('no-show').next().addClass('is-expand');
        $('.js-burger-link').not(this).addClass('no-show');
        $('.js-burger-link').not(this).removeClass('is-expand').next().removeClass('is-expand');
    }
});

$('.js-back').on('click', function () {
    var $wrap = $(this).parent();

    $wrap.removeClass('is-expand');
    $wrap.siblings().removeClass('is-expand');
    $('.b-burger__list').removeClass('open');
});

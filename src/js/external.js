// external scripts

// Модернизр
@@include('external/modernizr-custom.js')
@@include('node_modules/jquery/dist/jquery.min.js')
@@include('external/ofi.min.js')
@@include('node_modules/slick-carousel/slick/slick.js')
@@include('external/jquery.inputmask.min.js')
@@include('node_modules/jquery-validation/dist/jquery.validate.min.js')
@@include('external/tooltipster.bundle.min.js')
@@include('node_modules/overlayscrollbars/js/jquery.overlayScrollbars.js')
@@include('external/jquery.event.move.js')
@@include('external/jquery.twentytwenty.js')
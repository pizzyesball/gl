(function() {

  function documentReady() {
    @@include('partials/utilites/mediaHandler.js');
    @@include('partials/utilites/ClassEvent.js');
    @@include('partials/utilites/defineSetter.js');
    @@include('partials/utilites/AnimationQueue.js');
    @@include('partials/utilites/onscrollAnimate.js');

    @@include('partials/popup.js');
    @@include('partials/ie-detect.js');
    @@include('partials/slider.js');
    @@include('partials/work-with-us-slider.js');
    @@include('partials/tabs.js');
    @@include('partials/certificates-slider.js');
    @@include('partials/up.js');
    @@include('partials/js-burger.js');
    @@include('partials/header.js');
    @@include('partials/yamaps.js');
    @@include('partials/nav.js');
    @@include('partials/inputmask.js');
    @@include('partials/validation.js');
    @@include('partials/directions-slider.js');
    @@include('partials/tooltip.js');
    @@include('partials/prod-slider.js');
    @@include('partials/achieve-slider.js');
    @@include('partials/input.js');
    @@include('partials/scroll.js');
    @@include('partials/video.js');
    @@include('partials/values.js');
    @@include('partials/image-slider.js');
    @@include('partials/accordion.js');
    @@include('partials/filter.js');
    @@include('partials/sorting.js');
    @@include('partials/more-slider.js');
    @@include('partials/before-after.js');
    @@include('partials/object-fit.js');
  };

  document.addEventListener("DOMContentLoaded", documentReady);

})();